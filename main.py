from PyQt5 import QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import win32con  # 定義
import win32gui  # 界面
import win32api
import time  # 時間

import hide_window_ui as ui

class Main(QMainWindow, ui.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.pushButton.clicked.connect(self.hide_window)
        self.pushButton_2.clicked.connect(self.show_window)

    def hide_window(self):
        label = self.lineEdit.text()
        hwnd = win32gui.FindWindow(None, label)
        win32gui.ShowWindow(hwnd , win32con.SW_HIDE)  # 設置隱藏

    def show_window(self):
        label = self.lineEdit.text()
        hwnd = win32gui.FindWindow(None, label)
        win32gui.ShowWindow(hwnd , win32con.SW_SHOW)  # 設置隱藏
                

if __name__ == '__main__':
    import sys
    app = QtWidgets.QApplication(sys.argv)
    window = Main()
    window.show()
    sys.exit(app.exec_())